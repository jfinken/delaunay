package main

import (
	"bufio"
	"fmt"
	"github.com/fogleman/ln/ln"
	"os"
)

func print(str string) {
	fmt.Printf(str + "\n")
}
func printf(w *bufio.Writer, str string) {
	// TODO: error handling
	_, _ = w.WriteString(str + "\n")
	w.Flush()
}

// Render the 3D scene as a 2D SVG
func (data *Data) render3D(width, height float64) {

	scene := ln.Scene{}
	// add triangles
	for _, t := range data.triangles {

		tri := ln.NewTriangle(ln.Vector{t.p1.X(), t.p1.Y(), t.p1.Z()},
			ln.Vector{t.p2.X(), t.p2.Y(), t.p2.Z()},
			ln.Vector{t.p3.X(), t.p3.Y(), t.p3.Z()})
		scene.Add(tri)
	}
	// define camera parameters
	eye := ln.Vector{-width / 4.0, -height / 4.0, 10.0} // camera position
	center := ln.Vector{0, 0, -1.0}                     // camera looks at
	up := ln.Vector{0, 0, 1}                            // up direction

	// define rendering parameters
	fovy := 60.0   // vertical field of view, degrees
	znear := 0.1   // near z plane
	zfar := 2000.0 // far z plane
	step := 0.01   // how finely to chop the paths for visibility testing

	// compute 2D paths that depict the 3D scene
	paths := scene.Render(eye, center, up, width, height, fovy, znear, zfar, step)

	// render the paths in an image
	paths.WriteToPNG("go-delaunay.png", width, height)
}

// Print the geometry to stdout suitable for pasting into your local copy
// of Processing
func (data *Data) renderProcessing(width, height int) {

	//--------------------------------------------------------------------------
	// Three files:
	//	- 2D points
	//	- 2D triangles
	//	- 3D triangles
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// 2D points:
	//--------------------------------------------------------------------------
	f, _ := os.Create("delaunay.processing.2d.points")
	w := bufio.NewWriter(f)
	printf(w, fmt.Sprintf("void setup() {size(%d,%d); background(0);}", width, height))
	printf(w, fmt.Sprintf("void draw() {"))
	for _, pt := range data.verts {
		printf(w, fmt.Sprintf("rect(%f, %f, 3, 3);", pt.X(), pt.Y()))
	}
	printf(w, fmt.Sprintf("}"))
	f.Close()

	//--------------------------------------------------------------------------
	// 2D triangles:
	//--------------------------------------------------------------------------
	dimension := 2
	f, _ = os.Create("delaunay.processing.2d.triangles")
	w = bufio.NewWriter(f)
	printTriangles(w, data, width, height, dimension)
	f.Close()

	//--------------------------------------------------------------------------
	// 3D triangles:
	//--------------------------------------------------------------------------
	dimension = 3
	f, _ = os.Create("delaunay.processing.3d.triangles")
	w = bufio.NewWriter(f)
	printTriangles(w, data, width, height, dimension)
	f.Close()
}

func printTriangles(w *bufio.Writer, data *Data, width, height, dimension int) {
	// setup
	printf(w, fmt.Sprintf("//num points: %d, num triangles: %d", len(data.verts), len(data.triangles)))
	//3D setup
	if dimension == 3 {
		printf(w, fmt.Sprintf("void setup() {size(%d,%d, P3D); background(0);}", width, height))
		printf(w, fmt.Sprintf("void draw() {lights();translate(%d/18.0, %d/4.0, -%d/2);",
			width, height, width))
		printf(w, "rotateX(PI/4);rotateZ(-PI/6);")
	} else {
		printf(w, fmt.Sprintf("void setup() {size(%d,%d); background(0);}", width, height))
		printf(w, fmt.Sprintf("void draw() {"))
	}

	printf(w, "beginShape(TRIANGLES);")

	// Processing is JVM-based and thus has a 65k limit on the number of chars in strings
	n := 0
	draw := 0
	for _, tri := range data.triangles {
		if dimension == 2 {
			printf(w, fmt.Sprintf("vertex(%f,%f);", tri.p1.X(), tri.p1.Y()))
			printf(w, fmt.Sprintf("vertex(%f,%f);", tri.p2.X(), tri.p2.Y()))
			printf(w, fmt.Sprintf("vertex(%f,%f);", tri.p3.X(), tri.p3.Y()))
		} else {
			printf(w, fmt.Sprintf("vertex(%f,%f,%f);", tri.p1.X(), tri.p1.Y(), tri.p1.Z()))
			printf(w, fmt.Sprintf("vertex(%f,%f,%f);", tri.p2.X(), tri.p2.Y(), tri.p2.Z()))
			printf(w, fmt.Sprintf("vertex(%f,%f,%f);", tri.p3.X(), tri.p3.Y(), tri.p3.Z()))
		}

		if n > 1000 {
			printf(w, "endShape();")
			printf(w, fmt.Sprintf("draw%d();}", draw))
			printf(w, fmt.Sprintf("void draw%d() {", draw))
			printf(w, "beginShape(TRIANGLES);")
			n = 0
			draw++
		}
		n = n + 3
	}

	printf(w, "endShape(); }")

}
