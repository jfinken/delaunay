package main

import (
	m "github.com/go-gl/mathgl/mgl64"
	"math"
)

// Shape defines the behavior of geometry of this package.
type Shape interface {
	containsVertex(v m.Vec3) bool
	circumCircleContains(v m.Vec3) bool
}

// Edge holds the endpoints of edges of shapes within this package.
type Edge struct {
	p1, p2 m.Vec3
}

// Triangle holds vertices and edges of triangles during delaunay triangulation.
type Triangle struct {
	p1, p2, p3 m.Vec3
	e1, e2, e3 Edge
	complete   bool
}

func (t Triangle) in(s []Triangle) bool {
	for _, tri := range s {
		if tri == t {
			return true
		}
	}
	return false
}

// NewTriangle is a constructor for Triangles.  It helps assert triangle vertices
// and edges are assigned properly.
func NewTriangle(_p1, _p2, _p3 m.Vec3) *Triangle {
	t := &Triangle{p1: _p1, p2: _p2, p3: _p3,
		e1:       Edge{p1: _p1, p2: _p2},
		e2:       Edge{p1: _p2, p2: _p3},
		e3:       Edge{p1: _p3, p2: _p1},
		complete: false}
	return t
}
func (t *Triangle) containsVertex(v m.Vec3) bool {
	return t.p1 == v || t.p2 == v || t.p3 == v
}

// circumCenter returns the cartesian coordinates of the center of the
// circum-circle of the triangle.
func (t *Triangle) circumCenter() m.Vec3 {
	// Calculate the cartesian coordinates of the circum-center
	// Ref: https://en.wikipedia.org/wiki/Circumscribed_circle (see cartesian coordinates of circum-center)
	/*
	   D = 2 * [A_x(B_y - C_y) + B_x(C_y - A_y) + C_x(A_y - B_y)]
	   U_x = [(A_x^2 + A_y^2)(B_y - C_y) + (B_x^2 + B_y^2)(C_y - A_y) + (C_x^2 + C_y^2)(A_y - B_y)] / D
	   U_y = [(A_x^2 + A_y^2)(C_x - B_x) + (B_x^2 + B_y^2)(A_x - C_x) + (C_x^2 + C_y^2)(B_x - A_x)]/ D
	*/
	ab := math.Pow(t.p1.X(), 2) + math.Pow(t.p1.Y(), 2)
	cd := math.Pow(t.p2.X(), 2) + math.Pow(t.p2.Y(), 2)
	ef := math.Pow(t.p3.X(), 2) + math.Pow(t.p3.Y(), 2)

	D := 2 * ((t.p1.X() * (t.p2.Y() - t.p3.Y())) + (t.p2.X() * (t.p3.Y() - t.p1.Y())) + (t.p3.X() * (t.p1.Y() - t.p2.Y())))
	circumX := (ab*(t.p2.Y()-t.p3.Y()) + cd*(t.p3.Y()-t.p1.Y()) + ef*(t.p1.Y()-t.p2.Y())) / D
	circumY := (ab*(t.p3.X()-t.p2.X()) + cd*(t.p1.X()-t.p3.X()) + ef*(t.p2.X()-t.p1.X())) / D

	return m.Vec3{circumX, circumY}
}

// CircumCircleContains tests whether or not the unique circle that passes
// through each of the triangle's three vertices contains the given point v.
func (t *Triangle) CircumCircleContains(v m.Vec3) (bool, bool) {

	// Calculate the cartesian coordinates of the circum-center
	circumCenter := t.circumCenter()
	// Via Pythagoras measure the distance between the point and the center-point
	// Return:
	//  - whether it's less than or eq to the radius
	//  - second bool is a potential optimization.  TODO...
	distSq := math.Pow((v.X()-circumCenter.X()), 2.0) + math.Pow((v.Y()-circumCenter.Y()), 2.0)
	circumRadiusSq := math.Pow((t.p1.X()-circumCenter.X()), 2.0) + math.Pow((t.p1.Y()-circumCenter.Y()), 2.0)
	return (distSq <= circumRadiusSq), (circumCenter.X()+math.Sqrt(circumRadiusSq) < v.X())
}
