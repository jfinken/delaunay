package main

import (
	m "github.com/go-gl/mathgl/mgl64"
	_ "math"
	"testing"
)

func TestContainsVertex(t *testing.T) {
	p1 := m.Vec3{10, 20}
	p2 := m.Vec3{20, 20}
	p3 := m.Vec3{15, 25}
	tri := NewTriangle(p1, p2, p3)

	// true positive
	queryPt := m.Vec3{15, 25}
	expectation := true

	got := tri.containsVertex(queryPt)
	if got != expectation {
		t.Errorf("TestContainsVertex error! Got %t, Expected %t", got, expectation)
	}

	// true negative
	queryPt = m.Vec3{105, 250}
	expectation = false

	got = tri.containsVertex(queryPt)
	if got != expectation {
		t.Errorf("TestContainsVertex error! Got %t, Expected %t", got, expectation)
	}
}
func TestCircumCircle(t *testing.T) {
	p1 := m.Vec3{10, 20}
	p2 := m.Vec3{20, 20}
	p3 := m.Vec3{15, 25}
	tri := NewTriangle(p1, p2, p3)

	expectedCenter := m.Vec3{15, 20}

	got := tri.circumCenter()
	if got != expectedCenter {
		t.Errorf("TestCircumCenter error! Got %v, Expected %v", got, expectedCenter)
	}
}
func TestCircumCircleContains(t *testing.T) {

	p1 := m.Vec3{10, 20}
	p2 := m.Vec3{20, 20}
	p3 := m.Vec3{15, 25}
	tri := NewTriangle(p1, p2, p3)

	queryPt := m.Vec3{400.0, 500.0}
	circumCenter := m.Vec3{15, 20}

	// circum-center is definitely in
	expected := true
	inside, _ := tri.CircumCircleContains(circumCenter)
	if inside != expected {
		t.Errorf("TestCircumCircleContains error! Got %t, Expected %t", inside, expected)
	}
	// query-pt is not
	expected = false
	inside, _ = tri.CircumCircleContains(queryPt)
	if inside != expected {
		t.Errorf("TestCircumCircleContains error! Got %t, Expected %t", inside, expected)
	}
}
