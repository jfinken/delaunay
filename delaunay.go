package main

import (
	//"fmt"
	m "github.com/go-gl/mathgl/mgl64"
	"math"
)

// Data will hold the input vertices and output triangles
type Data struct {
	verts     []m.Vec3
	triangles []Triangle
}

// Triangulate computes the delaunay triangulation via Bowyer-Watson.  The
// resulting triangles are stored in the struct itself.
func (data *Data) Triangulate() {

	// Determine the super triangle: a triangle large enough to completely
	// contain all the points in the input vertex list
	minX := data.verts[0].X()
	minY := data.verts[0].Y()
	maxX := minX
	maxY := minY
	// TODO: golang's qsort
	for _, pt := range data.verts {
		if pt.X() < minX {
			minX = pt.X()
		}
		if pt.Y() < minY {
			minY = pt.Y()
		}
		if pt.X() > maxX {
			maxX = pt.X()
		}
		if pt.Y() > maxY {
			maxY = pt.Y()
		}
	}

	dx := maxX - minX
	dy := maxY - minY
	deltaMax := math.Max(dx, dy)
	midx := (minX + maxX) / 2.0
	midy := (minY + maxY) / 2.0

	scale := 3.0
	superP1 := m.Vec3{midx - scale*deltaMax, midy - deltaMax}
	superP2 := m.Vec3{midx, midy + scale*deltaMax}
	superP3 := m.Vec3{midx + scale*deltaMax, midy - deltaMax}

	tri := NewTriangle(superP1, superP2, superP3)
	data.triangles = append(data.triangles, *tri)

	for _, pt := range data.verts {

		var badTriangles []Triangle
		var edges []Edge

		for _, tri := range data.triangles {
			// modest optimization
			if tri.complete == true {
				continue
			}
			// check if the circum circle contains the point
			inside, optimization := tri.CircumCircleContains(pt)
			tri.complete = optimization
			if inside {
				//fmt.Printf("Pushing bad triangle.  Circum-circle of T contains point: %v\n", pt)
				badTriangles = append(badTriangles, tri)
				edges = append(edges, tri.e1)
				edges = append(edges, tri.e2)
				edges = append(edges, tri.e3)
			}
		}
		// Remove each bad-triangle from data.triangles
		var tempTris []Triangle
		for _, tri := range data.triangles {
			if !tri.in(badTriangles) {
				tempTris = append(tempTris, tri)
			}
		}
		data.triangles = tempTris

		// mark doubly specified edges within the edge buffer. This should only
		// leave the edges of the enclosing polygon.
		numEdges := len(edges)
		for j := 0; j < numEdges-1; j++ {
			for k := j + 1; k < numEdges; k++ {
				if (edges[j].p1 == edges[k].p2) && (edges[j].p2 == edges[k].p1) {
					edges[j].p1 = m.Vec3{-1, -1}
					edges[j].p2 = m.Vec3{-1, -1}
					edges[k].p1 = m.Vec3{-1, -1}
					edges[k].p2 = m.Vec3{-1, -1}
				}
				/*
					// TODO: required??
					if (edges[j].p1 == edges[k].p1) && (edges[j].p2 == edges[k].p2) {
						edges[j].p1 = m.Vec2{-1, -1}
						edges[j].p2 = m.Vec2{-1, -1}
						edges[k].p1 = m.Vec2{-1, -1}
						edges[k].p2 = m.Vec2{-1, -1}
					}
				*/
			}
		}
		// Generate new triangles with the valid edges
		for _, edge := range edges {
			if (edge.p1.X() == -1 && edge.p1.Y() == -1) ||
				(edge.p2.X() == -1 && edge.p2.Y() == -1) {
				continue
			}
			newTri := NewTriangle(edge.p1, edge.p2, pt)
			data.triangles = append(data.triangles, *newTri)
		}
	}
	// if any triangle contains a vertex from original super-triangle remove
	// the triangle from triangulation
	// TODO: optimize?
	var tempTris []Triangle
	for _, tri := range data.triangles {
		if !tri.containsVertex(superP1) && !tri.containsVertex(superP2) && !tri.containsVertex(superP3) {
			tempTris = append(tempTris, tri)
		}
	}
	// DONE: data.triangles
	data.triangles = tempTris
}
