package main

import (
	_ "fmt"
	m "github.com/go-gl/mathgl/mgl64"
	n "github.com/ojrac/opensimplex-go"
	"math/rand"
	"time"
)

func main() {

	width := 1024.0
	height := 768.0
	// Generate a set of points
	data := &Data{}
	rand.Seed(time.Now().UTC().UnixNano())
	numPoints := randInt(3000, 4000)

	//-------------------------------------------------------------------------
	// - R^3 vectors are used however the triangulation implementation is 2D.
	// - an open-simplex implementation is used to generate z-components (for fun)
	//-------------------------------------------------------------------------
	noise := n.NewWithSeed(0)
	for i := 0; i < numPoints; i++ {
		x := randFloat(0.0, width)
		y := randFloat(0.0, height)
		z := newZ(noise, x, y, 0.0, height/15.0)
		pt := m.Vec3{x, y, z}
		data.verts = append(data.verts, pt)
	}

	data.Triangulate()
	// to stdout
	data.renderProcessing(int(width), int(height))
	// to png
	//data.render3D(width, height)

	//fmt.Printf("Num points: %d, triangles generated: %d\n", numPoints, len(data.triangles))
}
func randInt(min, max int) int {
	return min + rand.Intn(max-min)
}
func randFloat(min, max float64) float64 {
	return min + (rand.Float64() * max)
}

// Open-Simplex Noise-based Z generation
//  Reference:
//  http://www.redblobgames.com/maps/terrain-from-noise/
//  http://blog.ojrac.com/opensimplex-noise-in-go.html
func newZ(noise *n.Noise, x, y, min, max float64) float64 {

	//e := 1.0*noise.Eval2(1.0*x, 1.0*y) + 0.5*noise.Eval2(2.0*x, 2.0*y) + 0.25*noise.Eval2(4.0*x, 4.0*y)
	//return math.Pow(e, 0.4)
	z := noise.Eval2(x, y)
	return min + (z * max)
}
