# Delaunay Triangulation

## Implementation of Bowyer-Watson triangulation in Golang

 * [Wikipedia entry on BW](https://en.wikipedia.org/wiki/Bowyer%E2%80%93Watson_algorithm)
 * WIP:
      - rendering via stdout into [processing](https://processing.org)
      - rendering via [ln](https://github.com/fogleman/ln)

## Example:

#### Input Points: 3458
![Input Points](http://i.imgur.com/yvDyKKP.png)
#### Resulting triangulation: 3458 points, 6871 triangles
![Delaunay Triangulation](http://i.imgur.com/erW13Jv.png)
#### Resulting triangulation: 6871 triangles with Z values assigned via [OpenSimplex noise](https://en.wikipedia.org/wiki/OpenSimplex_noise)
![3D triangles z-values via Open-Simplex](http://i.imgur.com/h4Rt0Vx.png?1)
